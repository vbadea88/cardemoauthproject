package utils.enums;

import com.example.demo.constants.CarOwnerConstants;
import com.example.demo.dto.response.OwnerResponseDto;

import java.util.HashMap;

public enum ContainedPropertiesEnum {
    CAR(setCarContainedProperties());

    private HashMap<String, Class> properties;

    ContainedPropertiesEnum(HashMap<String, Class> properties) {
        this.properties = properties;
    }

    public  HashMap<String, Class> getProperties() {
        return properties;
    }

    private static HashMap<String, Class> setCarContainedProperties(){
        HashMap<String, Class> carMap = new HashMap<>();
        carMap.put(CarOwnerConstants.OWNER_RESOURCE_TYPE, OwnerResponseDto.class);
        return carMap;
    }
}
