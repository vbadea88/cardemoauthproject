/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package utils;

/**
 * This class is used to handle requests for the DBQuerryConstants entity.
 */

public class DBQueryConstants {
    public static final String SEARCH_OWNERS_BY_CAR_BRAND =
            "SELECT O.*, C.BRAND  FROM CARSCHEMA.OWNER AS O " +
            " LEFT JOIN CARSCHEMA.CARS AS C ON (O.CAR_ID = C.CAR_ID)" +
            "WHERE " +
            "LOWER(CAST (C.BRAND AS TEXT)) LIKE LOWER(concat('%', :carBrand, '%'))";
}