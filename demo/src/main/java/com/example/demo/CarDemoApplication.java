package com.example.demo;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CarDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarDemoApplication.class, args);
	}

	@Bean
	public ModelMapper createModelMapperBean(){
		return new ModelMapper();
	}
}
