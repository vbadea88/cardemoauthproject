/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.mapper;

import com.example.demo.dto.ContainedObject;
import com.example.demo.dto.create.CarCreateDto;
import com.example.demo.dto.response.CarResponseDto;
import com.example.demo.entities.CarEntity;
import com.example.demo.entities.OwnerEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to handle requests for the CarMapper entity.
 */
@Component
public class CarMapper {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private OwnerMapper ownerMapper;

    private TypeMap<CarCreateDto, CarEntity> typeMapCarDtoToEntity;
    private TypeMap<CarEntity, CarResponseDto> typeMapCarEntityToDto;

    public CarEntity convertCarDtotoEntity(CarCreateDto carDto){
        if(null == typeMapCarDtoToEntity){
            typeMapCarDtoToEntity = modelMapper.createTypeMap(CarCreateDto.class, CarEntity.class);
        }
        return typeMapCarDtoToEntity.map(carDto);
    }

    public CarResponseDto convertCarEntitytoResDto(CarEntity carEntity, String revInclude){

        if(null == typeMapCarEntityToDto){
            typeMapCarEntityToDto = modelMapper.createTypeMap(CarEntity.class, CarResponseDto.class);
        }

        CarResponseDto carResponseDto = typeMapCarEntityToDto.map(carEntity);

        if(!StringUtils.isEmpty(revInclude)){
            List<ContainedObject> ownerResponseDtoList = new ArrayList<>();

            for(OwnerEntity ownerEntity : carEntity.getOwnerEntitySet()){
                ownerResponseDtoList.add(ownerMapper.convertOwnerToContained(ownerEntity));
            }

            carResponseDto.setContained(ownerResponseDtoList);
        }

        return carResponseDto;
    }


}