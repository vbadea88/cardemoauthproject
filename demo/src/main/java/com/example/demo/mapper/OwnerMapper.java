/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.mapper;

import com.example.demo.dto.contained.OwnerContainedDto;
import com.example.demo.dto.create.OwnerCreateDto;
import com.example.demo.dto.response.OwnerCarBrandRDto;
import com.example.demo.dto.response.OwnerResponseDto;
import com.example.demo.entities.OwnerEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is used to handle requests for the OwnerMapper entity.
 */
@Component
public class OwnerMapper {
    @Autowired
    private ModelMapper modelMapper;

    private TypeMap<OwnerCreateDto, OwnerEntity> typeMapOwnerDtoToEntity;
    private TypeMap<OwnerEntity, OwnerResponseDto> typeMapOwnerEntityToDto;
    private TypeMap<OwnerEntity, OwnerCarBrandRDto> typeMapOwnerEntityToOwnerCarBrandDto;
    private TypeMap<OwnerEntity, OwnerContainedDto> typeMapOwnerEntityToOwnerContainedDto;

    public OwnerEntity convertOwnerDtoToEntity(OwnerCreateDto carDto){
        if(null == typeMapOwnerDtoToEntity){
            typeMapOwnerDtoToEntity = modelMapper.createTypeMap(OwnerCreateDto.class, OwnerEntity.class);
        }
        return typeMapOwnerDtoToEntity.map(carDto);
    }

    public OwnerResponseDto convertOwnerEntityToResDto(OwnerEntity ownerEntity){
        if(null == typeMapOwnerEntityToDto){
            typeMapOwnerEntityToDto = modelMapper.createTypeMap(OwnerEntity.class, OwnerResponseDto.class);
        }
        return typeMapOwnerEntityToDto.map(ownerEntity);
    }

    public OwnerCarBrandRDto convertOwnerToOCBDto(OwnerEntity ownerEntity){
        if(null == typeMapOwnerEntityToOwnerCarBrandDto){
            typeMapOwnerEntityToOwnerCarBrandDto = modelMapper.createTypeMap(OwnerEntity.class, OwnerCarBrandRDto.class);
        }

        return typeMapOwnerEntityToOwnerCarBrandDto.map(ownerEntity);
    }

    public OwnerContainedDto convertOwnerToContained(OwnerEntity ownerEntity){
        if(null == typeMapOwnerEntityToOwnerContainedDto){
            typeMapOwnerEntityToOwnerContainedDto = modelMapper.createTypeMap(OwnerEntity.class, OwnerContainedDto.class);
        }

        return typeMapOwnerEntityToOwnerContainedDto.map(ownerEntity);
    }
}