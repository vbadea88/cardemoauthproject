package com.example.demo.repository;

import com.example.demo.entities.CarEntity;
import com.example.demo.entities.OwnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import utils.DBQueryConstants;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OwnerRepository extends JpaRepository<OwnerEntity, UUID> {
    @Query(value = DBQueryConstants.SEARCH_OWNERS_BY_CAR_BRAND, nativeQuery = true)
    List<OwnerEntity> getOwnersWithCarName(
            @Param("carBrand") String carBrand
    );

    Optional<List<OwnerEntity>> findOwnerEntityByCarEntity(CarEntity carEntity);
}
