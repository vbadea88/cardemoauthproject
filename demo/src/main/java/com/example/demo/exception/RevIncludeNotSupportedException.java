/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.exception;

/**
 * This class is used to handle requests for the revIncludeNotSuppoerted entity.
 */

public class RevIncludeNotSupportedException extends RuntimeException{
    public RevIncludeNotSupportedException(String entityName, String revInclude) {
        super(String.format("The [%s] entity does not support [%s] revInclude", entityName, revInclude));
    }
}