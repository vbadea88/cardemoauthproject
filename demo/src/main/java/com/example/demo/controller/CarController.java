/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.controller;

import com.example.demo.constants.CarOwnerConstants;
import com.example.demo.dto.create.CarCreateDto;
import com.example.demo.dto.response.CarResponseDto;
import com.example.demo.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * This class is used to handle requests for the CarController entity.
 */
@RestController
@RequestMapping("/cars")
public class CarController {

    @Autowired
    private CarService carService;

    @PostMapping(produces = "application/json", consumes = "application/json")
    public ResponseEntity<CarResponseDto>createNewCar(@RequestBody CarCreateDto carDto){
        CarResponseDto createdCar = carService.createNewCar(carDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(createdCar);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<CarResponseDto>>getAllCars(@RequestHeader(value = CarOwnerConstants.REV_INCLUDE_HEADER,
            required = false) String revInclude){
        List<CarResponseDto> carDtos = carService.getAllCars(revInclude);
        return ResponseEntity.status(HttpStatus.OK)
                .body(carDtos);
    }

    @GetMapping(value = {"/{id}"}, produces = "application/json")
    public ResponseEntity<CarResponseDto>getCarsById(@RequestHeader(value = CarOwnerConstants.REV_INCLUDE_HEADER,
            required = false) String revInclude, @PathVariable String id){
        CarResponseDto carDto = carService.getCarById(id, revInclude);
        return ResponseEntity.status(HttpStatus.OK)
                .body(carDto);
    }

    @PutMapping(value = {"/{id}"}, produces = "application/json", consumes = "application/json")
    public ResponseEntity<CarResponseDto>updateCar(@PathVariable String id, @RequestBody CarCreateDto carDto){
        CarResponseDto carDtoResult = carService.updateCar(id, carDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(carDtoResult);
    }

    @DeleteMapping(value = {"/{id}"}, produces = "application/json")
    public ResponseEntity<CarResponseDto>deleteCarsById(@PathVariable String id){
        CarResponseDto carDto = carService.deleteCar(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(carDto);
    }
}