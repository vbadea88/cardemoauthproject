/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.controller;

import com.example.demo.dto.create.OwnerCreateDto;
import com.example.demo.dto.response.OwnerCarBrandRDto;
import com.example.demo.dto.response.OwnerResponseDto;
import com.example.demo.mapper.OwnerMapper;
import com.example.demo.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * This class is used to handle requests for the OwnerController entity.
 */
@RestController
@RequestMapping("/owners")
public class OwnerController {

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private OwnerMapper ownerMapper;

    @PostMapping(produces = "application/json", consumes = "application/json")
    public  ResponseEntity<OwnerResponseDto> createNewOwner(@RequestBody OwnerCreateDto ownerDto){
        OwnerResponseDto ownerResponseDto = ownerService.createNewOwner(ownerDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(ownerResponseDto);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<OwnerResponseDto>> getAllOwners(){
        List<OwnerResponseDto> ownerDtos = ownerService.getAllOwners();
        return ResponseEntity.status(HttpStatus.OK)
                .body(ownerDtos);
    }

    @GetMapping(value = {"/{id}"}, produces = "application/json")
    public ResponseEntity<OwnerResponseDto> getOwnerById(@PathVariable String id){
        OwnerResponseDto ownerDto = ownerService.getOwnerById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(ownerDto);
    }

    @PutMapping(value = {"/{id}"}, produces = "application/json", consumes = "application/json")
    public ResponseEntity<OwnerResponseDto> updateOwner(@PathVariable String id, @RequestBody OwnerCreateDto ownerDto){
        OwnerResponseDto updatedOwner = ownerService.updateOwner(id, ownerDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(updatedOwner);
    }

    @DeleteMapping(value = {"/{id}"}, produces = "application/json")
    public ResponseEntity<OwnerResponseDto>deleteOwner(@PathVariable String id){
        OwnerResponseDto deletedOwner = ownerService.deleteOwner(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(deletedOwner);
    }

    @GetMapping(value = "/getOwnerWithCarName", produces = "application/json")
    public ResponseEntity<List<OwnerCarBrandRDto>>getOwnerWithCarName(@RequestParam String carBrand){
        List<OwnerCarBrandRDto> carResponseDtoList = ownerService.getOwnerWithCarName(carBrand);
        return ResponseEntity.status(HttpStatus.OK)
                .body(carResponseDtoList);
    }

    @GetMapping(value = "/getOwnerWithCarNameJpa", produces = "application/json")
    public ResponseEntity<List<OwnerCarBrandRDto>>getOwnerWithCarNameJpa(@RequestParam String carBrand){
        List<OwnerCarBrandRDto> carResponseDtoList = ownerService.getOwnerWithCarNameJpa(carBrand);
        return ResponseEntity.status(HttpStatus.OK)
                .body(carResponseDtoList);
    }
}