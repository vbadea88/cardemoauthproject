/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

/**
 * This class is used to handle requests for the OwnerEntity entity.
 */
@Entity
@Table(name = "Owner")
public class OwnerEntity {

    @Id
    @Column(name = "owner_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @GenericGenerator(name = "owner_id_generator", strategy = "uuid2", parameters = @Parameter(name = "uuid_gen_strategy_class", value = "com.xxx.xxx.xxx.xxx.PostgreSQLUUIDGenerationStrategy"))
    private UUID ownerId;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "rich")
    private Boolean isRich;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "car_id", nullable = false)
    private CarEntity carEntity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getRich() {
        return isRich;
    }

    public void setRich(Boolean rich) {
        isRich = rich;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public CarEntity getCarEntity() {
        return carEntity;
    }

    public void setCarEntity(CarEntity carEntity) {
        this.carEntity = carEntity;
    }
}