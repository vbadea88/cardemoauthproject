/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

/**
 * This class is used to handle requests for the CarEntity entity.
 */
@Entity
@Table(name = "Cars")
public class CarEntity {

    @Id
    @Column(name = "car_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @GenericGenerator(name = "car_id_generator", strategy = "uuid2", parameters = @Parameter(name = "uuid_gen_strategy_class", value = "com.xxx.xxx.xxx.xxx.PostgreSQLUUIDGenerationStrategy"))
    private UUID carId;

    @Column(name = "brand")
    private String brand;

    @OneToMany(mappedBy = "carEntity")
    @JsonManagedReference
    private Set<OwnerEntity> ownerEntitySet;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public UUID getCarId() {
        return carId;
    }

    public void setCarId(UUID carId) {
        this.carId = carId;
    }

    public Set<OwnerEntity> getOwnerEntitySet() {
        return ownerEntitySet;
    }

    public void setOwnerEntitySet(Set<OwnerEntity> ownerEntitySet) {
        this.ownerEntitySet = ownerEntitySet;
    }
}