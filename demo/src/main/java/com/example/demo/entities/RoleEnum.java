package com.example.demo.entities;

public enum RoleEnum {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
