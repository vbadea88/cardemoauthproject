package com.example.demo.service;

import com.example.demo.dto.create.CarCreateDto;
import com.example.demo.dto.response.CarResponseDto;

import java.util.List;

public interface CarService {
    CarResponseDto createNewCar(CarCreateDto carDto);
    CarResponseDto getCarById(String id, String revInclude);
    CarResponseDto updateCar(String id, CarCreateDto carDto);
    CarResponseDto deleteCar(String id);
    List<CarResponseDto> getAllCars(String revInclude);
}
