/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.service.impl;

import com.example.demo.constants.CarOwnerConstants;
import com.example.demo.dto.create.OwnerCreateDto;
import com.example.demo.dto.response.OwnerCarBrandRDto;
import com.example.demo.dto.response.OwnerResponseDto;
import com.example.demo.entities.CarEntity;
import com.example.demo.entities.OwnerEntity;
import com.example.demo.exception.RecordNotFoundException;
import com.example.demo.mapper.OwnerMapper;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.OwnerRepository;
import com.example.demo.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * This class is used to handle requests for the OwnerServiceImpl entity.
 */
@Service
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private OwnerMapper ownerMapper;


    @Override
    public OwnerResponseDto createNewOwner(OwnerCreateDto carDto) {
        OwnerEntity ownerEntity = ownerMapper.convertOwnerDtoToEntity(carDto);

        CarEntity carEntity = null;
        if (!StringUtils.isEmpty(carDto.getCarId())) {
            Optional<CarEntity> optionalCarEntity = carRepository.findById(UUID.fromString(carDto.getCarId()));
            if (!optionalCarEntity.isPresent()) {
                throw new RecordNotFoundException(String.format(CarOwnerConstants.CAR_ID_NOT_PRESENT, carDto.getCarId()));
            }
            carEntity = optionalCarEntity.get();
        }

        ownerEntity.setCarEntity(carEntity);

        return ownerMapper.convertOwnerEntityToResDto(ownerRepository.save(ownerEntity));
    }

    @Override
    public OwnerResponseDto getOwnerById(String id) {
        Optional<OwnerEntity> op = ownerRepository.findById(UUID.fromString(id));

        if (!op.isPresent()) {
            throw new RecordNotFoundException(String.format(CarOwnerConstants.OWNER_ID_NOT_PRESENT, id));
        }
        OwnerEntity ownerEntity = op.get();

        return ownerMapper.convertOwnerEntityToResDto(ownerEntity);
    }

    @Override
    public OwnerResponseDto updateOwner(String id, OwnerCreateDto ownerDto) {
        Optional<OwnerEntity> op = ownerRepository.findById(UUID.fromString(id));

        if (!op.isPresent()) {
            throw new RecordNotFoundException(String.format(CarOwnerConstants.OWNER_ID_NOT_PRESENT, id));
        }
        OwnerEntity ownerEntityInDb = op.get();

        CarEntity carEntity = null;
        if (!StringUtils.isEmpty(ownerDto.getCarId())) {
            Optional<CarEntity> optionalCarEntity = carRepository.findById(UUID.fromString(ownerDto.getCarId()));
            if (!optionalCarEntity.isPresent()) {
                throw new RecordNotFoundException(String.format(CarOwnerConstants.CAR_ID_NOT_PRESENT, ownerDto.getCarId()));
            }
            carEntity = optionalCarEntity.get();
        } else {
            carEntity = ownerEntityInDb.getCarEntity();
        }

        OwnerEntity toSave = ownerMapper.convertOwnerDtoToEntity(ownerDto);
        toSave.setOwnerId(ownerEntityInDb.getOwnerId());
        toSave.setCarEntity(carEntity);

        return ownerMapper.convertOwnerEntityToResDto(ownerRepository.save(toSave));
    }

    @Override
    public OwnerResponseDto deleteOwner(String id) {
        Optional<OwnerEntity> op = ownerRepository.findById(UUID.fromString(id));

        if (!op.isPresent()) {
            throw new RecordNotFoundException(String.format(CarOwnerConstants.OWNER_ID_NOT_PRESENT, id));
        }
        OwnerEntity ownerEntity = op.get();
        ownerRepository.delete(ownerEntity);

        return ownerMapper.convertOwnerEntityToResDto(ownerEntity);
    }

    @Override
    public List<OwnerResponseDto> getAllOwners() {
        return ownerRepository.findAll().stream().map(ownerEntity -> ownerMapper.convertOwnerEntityToResDto(ownerEntity)).collect(Collectors.toList());
    }

    public List<OwnerCarBrandRDto> getOwnerWithCarName(String carBrand) {
        return ownerRepository.getOwnersWithCarName(carBrand).stream().map(ownerEntity -> ownerMapper.convertOwnerToOCBDto(ownerEntity)).collect(Collectors.toList());
    }

    public List<OwnerCarBrandRDto> getOwnerWithCarNameJpa(String carBrand) {
        List<OwnerCarBrandRDto> ownerCarBrandRDtos = new LinkedList<>();
        Optional<CarEntity> carEntityOpt = carRepository.findByBrand(carBrand);
        if (carEntityOpt.isPresent()) {
            CarEntity carEntity = carEntityOpt.get();

            Optional<List<OwnerEntity>> ownerEntityOptional = ownerRepository.findOwnerEntityByCarEntity(carEntity);
            if (ownerEntityOptional.isPresent()) {
                List<OwnerEntity> ownerEntityList = ownerEntityOptional.get();
                return ownerEntityList.stream().map(ownerEntity -> ownerMapper.convertOwnerToOCBDto(ownerEntity)).collect(Collectors.toList());
            }
        }
        return ownerCarBrandRDtos;
    }
}