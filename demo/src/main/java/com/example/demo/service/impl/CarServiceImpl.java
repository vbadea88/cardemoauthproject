/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.service.impl;

import com.example.demo.constants.CarOwnerConstants;
import com.example.demo.dto.create.CarCreateDto;
import com.example.demo.dto.response.CarResponseDto;
import com.example.demo.entities.CarEntity;
import com.example.demo.exception.RecordNotFoundException;
import com.example.demo.exception.RevIncludeNotSupportedException;
import com.example.demo.mapper.CarMapper;
import com.example.demo.mapper.OwnerMapper;
import com.example.demo.repository.CarRepository;
import com.example.demo.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import utils.enums.ContainedPropertiesEnum;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * This class is used to handle requests for the CarServiceImpl entity.
 */
@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarMapper carMapper;

    @Autowired
    private OwnerMapper ownerMapper;

    @Override
    public CarResponseDto createNewCar(CarCreateDto carDto) {
        CarEntity carEntity= carRepository.save(carMapper.convertCarDtotoEntity(carDto));
        return carMapper.convertCarEntitytoResDto(carEntity, "");
    }

    @Override
    public CarResponseDto getCarById(String id, String revInclude) {
        Optional<CarEntity> op = carRepository.findById(UUID.fromString(id));

        if(!op.isPresent()){
            throw new RecordNotFoundException(String.format("Car id [%s] not found in Database", id));
        }
        CarEntity carEntity = op.get();
        return carMapper.convertCarEntitytoResDto(carEntity, revInclude);
    }

    @Override
    public CarResponseDto updateCar(String id, CarCreateDto carDto) {
        Optional<CarEntity> op = carRepository.findById(UUID.fromString(id));

        if(!op.isPresent()){
            throw new RecordNotFoundException(String.format(CarOwnerConstants.CAR_ID_NOT_PRESENT, id));
        }
        CarEntity toSave = op.get();
        CarEntity carToSave = carMapper.convertCarDtotoEntity(carDto);
        carToSave.setCarId(toSave.getCarId());

        return carMapper.convertCarEntitytoResDto(carRepository.save(carToSave), "");
    }

    @Override
    public CarResponseDto deleteCar(String id) {
        Optional<CarEntity> op = carRepository.findById(UUID.fromString(id));

        if(!op.isPresent()){
            throw new RecordNotFoundException(String.format(CarOwnerConstants.CAR_ID_NOT_PRESENT, id));
        }
        CarEntity carEntity = op.get();
        carRepository.delete(carEntity);

        return carMapper.convertCarEntitytoResDto(carEntity, "");
    }

    @Override
    public List<CarResponseDto> getAllCars(String revInclude) {
        validateCarRevInclude(revInclude);
        return carRepository.findAll().stream().map(carEntity -> carMapper.convertCarEntitytoResDto(carEntity, revInclude)).collect(Collectors.toList());
    }

    private void validateCarRevInclude(String revInclude){
        if(!StringUtils.isEmpty(revInclude)){
            boolean revIncludeIncluded = ContainedPropertiesEnum.CAR.getProperties().containsKey(revInclude);
            if(!revIncludeIncluded){
                throw new RevIncludeNotSupportedException(CarOwnerConstants.CAR_RESOURCE_TYPE, revInclude);
            }
        }
    }

}