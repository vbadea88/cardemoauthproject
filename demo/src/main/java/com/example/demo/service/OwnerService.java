package com.example.demo.service;

import com.example.demo.dto.create.OwnerCreateDto;
import com.example.demo.dto.response.OwnerCarBrandRDto;
import com.example.demo.dto.response.OwnerResponseDto;

import java.util.List;

public interface OwnerService {
    OwnerResponseDto createNewOwner(OwnerCreateDto carDto);
    OwnerResponseDto getOwnerById(String id);
    OwnerResponseDto updateOwner(String id, OwnerCreateDto ownerDto);
    OwnerResponseDto deleteOwner(String id);
    List<OwnerResponseDto> getAllOwners();
    List<OwnerCarBrandRDto> getOwnerWithCarName(String carBrand);
    List<OwnerCarBrandRDto> getOwnerWithCarNameJpa(String carBrand);
}
