package com.example.demo.service;

import com.example.demo.payload.request.LoginRequest;
import com.example.demo.payload.request.SignUpRequest;
import com.example.demo.payload.response.JwtResponse;

public interface AuthService {
    public void registerUser(SignUpRequest signUpRequest);
    public JwtResponse authenticateUser(LoginRequest loginRequest);
}
