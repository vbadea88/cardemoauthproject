/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.constants;

/**
 * This class is used to handle requests for the CarOwnerConstants entity.
 */

public abstract class CarOwnerConstants {
    public static final String OWNER_ID_NOT_PRESENT = "Owner Id [%s] not present in Database";
    public static final String CAR_ID_NOT_PRESENT = "Car Id [%s] not present in Database";
    public static final String SUCCESSFULL_REGISTRATION = "User registered successfully!";
    public static final String ROLE_NOT_FOUND = "Error: Role is not found.";

    public static final String REV_INCLUDE_HEADER = "_revInclude";

    public static final String OWNER_RESOURCE_TYPE = "Owner";
    public static final String CAR_RESOURCE_TYPE = "Car";

    private CarOwnerConstants() {
    }
}