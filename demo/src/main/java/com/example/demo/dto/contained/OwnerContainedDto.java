/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.dto.contained;

import com.example.demo.dto.ContainedObject;
import com.example.demo.dto.response.OwnerResponseDto;

/**
 * This class is used to handle requests for the OwnerContainedDto entity.
 */

public class OwnerContainedDto extends OwnerResponseDto implements ContainedObject {
}