/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.dto.create;

/**
 * This class is used to handle requests for the OwnerDto entity.
 */

public class OwnerCreateDto {
    private String name;
    private String age;
    private boolean isRich;
    private String carId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public boolean getRich() {
        return isRich;
    }

    public void setRich(boolean rich) {
        isRich = rich;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }
}