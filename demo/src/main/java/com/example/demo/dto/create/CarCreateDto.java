/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.dto.create;

/**
 * This class is used to handle requests for the CarDto entity.
 */

public class CarCreateDto {
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}