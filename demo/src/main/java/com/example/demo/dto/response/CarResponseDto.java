/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.dto.response;

import com.example.demo.dto.ContainedObject;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to handle requests for the CarDto entity.
 */

public class CarResponseDto {
    private String carId;
    private String brand;
    @Transient
    private List<ContainedObject> contained = new ArrayList<>();

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public List<ContainedObject> getContained() {
        return contained;
    }

    public void setContained(List<ContainedObject> contained) {
        this.contained = contained;
    }
}