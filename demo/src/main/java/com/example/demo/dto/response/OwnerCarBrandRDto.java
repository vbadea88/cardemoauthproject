/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.dto.response;

/**
 * This class is used to handle requests for the OwnerCarBrandRDto entity.
 */

public class OwnerCarBrandRDto extends OwnerResponseDto {
    private String carBrand;

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }
}